import React, { Component } from 'react';
import './App.css';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost/Symfony/Projet_Symfo_React/form_symfony/public/index.php/api/t_emails")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <table>
        <thead>
      <td><th>Nom</th></td>
      <td> <th>Prénom</th></td>
      <td> <th>N° tel</th></td>
      <td><th>Email</th></td>
      <td> <th>Sujet</th></td>
      <td> <th>Message</th></td>
      <td><th>Date</th></td>
      <td>  <th>Etat</th></td>
    </thead>
    
    <td>
      
        {items.map(item => (
            
            <tr key={item.id}>
              
              {item.tPeople[0].LastName}
              <hr></hr>
            
            </tr>
          ))}
        </td>

    <td>
    {items.map(item => (
            
            <tr key={item.id}>
              
              
              {item.tPeople[0].FirstName}<br></br>
              <hr></hr>
            
            </tr>
          ))}
    </td>

    <td>
    
    {items.map(item => (
            
            <tr key={item.id}>
              

              {item.tPeople[0].Phone}<br></br>
              <hr></hr>
            </tr>
          ))}
    
    </td>

        <td>
        {items.map(item => (
            
            <tr key={item.id}>
              
              {item.Email}<br></br>
              <hr></hr>
            
            </tr>
          ))}
        </td> 
        
        <td>
        {items.map(item => (
            
            <tr key={item.id}>
              {item.tMsgs[0].Subject}<br></br>
              <hr></hr>
            </tr>
          ))}
        </td>
        
        <td>
        {items.map(item => (
            
            <tr key={item.id}>
              

              {item.tMsgs[0].msg}<br></br>
              <hr></hr>
            </tr>
          ))}
        </td>
        
        <td>
        {items.map(item => (
            
            <tr key={item.id}>

              {item.tMsgs[0].Date}<br></br>
              <hr></hr>
            </tr>
          ))}
        </td>
        
      <td>
        {items.map(item => (
            
            <tr key={item.id}>

              {item.tMsgs[0].State}<br></br>
              <hr></hr>
            </tr>
          ))}
        </td>
        
        
      
        </table>
      );
    }
  }
}

export default App;
