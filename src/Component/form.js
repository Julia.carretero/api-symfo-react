import React, { Component } from 'react';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {
        Email: "",
        FirstName: "",
        LastName: "",
        Phone: "",
        Subjet: "",
        msg: "",
      }
    };
  }
  
  submitForm = e => {
    e.preventDefault();
    console.log(this.state);
    
    fetch("http://localhost/Symfony/Projet_Symfo_React/form_symfony/public/index.php/api/t_emails", {
      method: "POST",
      body: JSON.stringify({
        "Email": this.state.values.Email,
        "tMsgs": [
          {
            "Subject": this.state.values.Subject,
            "msg": this.state.values.msg,
            "Date": "2021-02-24T13:15:40.961Z",
            "State": "A Traiter"
          }
        ],
        "tPeople": [
          {
            "FirstName": this.state.values.FirstName,
            "LastName": this.state.values.LastName,
            "Phone": this.state.values.Phone
          }
        ]
      }),
      headers: {
        "Content-Type": "application/json",
        'Accept': 'application/json',
      }
    });
    setTimeout(
      () =>
        this.setState({
          values: {
            Email: "",
            FirstName: "",
            LastName: "",
            Phone: "",
            Subjet: "",
            msg: "",
          }
        }),
      1600
    );
  };

  handleInputChange = e =>
    this.setState({
      values: { ...this.state.values, [e.target.name]: e.target.value }
    });

  render() {
    return (
      <div>
        <form onSubmit={this.submitForm}>
          <div className="input-group">
            <label htmlFor="Email">E-mail Address</label>
            <input
              type="email"
              name="Email"
              id="Email"
              value={this.state.values.Email}
              onChange={this.handleInputChange}
              title="Email"
              required
            />
          </div>
          <div className="input-group">
            <label htmlFor="FirstName">First Name</label>
            <input
              type="text"
              name="FirstName"
              id="FirstName"
              value={this.state.values.FirstName}
              onChange={this.handleInputChange}
              title="FirstName"
              required
            />
          </div>
          <div className="input-group">
            <label htmlFor="LastName">Last Name</label>
            <input
              type="text"
              name="LastName"
              id="LastName"
              value={this.state.values.LastName}
              onChange={this.handleInputChange}
              title="LastName"
              required
            />
          </div>
          <div className="input-group">
            <label htmlFor="Phone">Phone</label>
            <input
              type="tel"
              name="Phone"
              id="Phone"
              value={this.state.values.Phone}
              onChange={this.handleInputChange}
              title="Phone"
              required
            />
          </div>
          <div className="input-group">
            <label htmlFor="Subject">Subject</label>
            <input
              type="text"
              name="Subject"
              id="Subject"
              value={this.state.values.Subject}
              onChange={this.handleInputChange}
              title="Subject"
              required
            />
            <div className="input-group">
            <label htmlFor="msg">Message</label>
            <textarea
              name="msg"
              id="msg"
              value={this.state.values.msg}
              onChange={this.handleInputChange}
              title="msg"
              required
            />
          </div>
          </div>


          <button type="submit">Sign In</button>
        </form>
      </div>
    );
  }
}

export default Form;